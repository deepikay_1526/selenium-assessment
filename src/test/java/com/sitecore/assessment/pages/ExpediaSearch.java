package com.sitecore.assessment.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.sitecore.assessment.utils.TestBase;
import com.sitecore.assessment.utils.TestUtil;

public class ExpediaSearch extends TestBase {
	
	@FindBy(xpath = "//div[@data-stid = 'language-picker-trigger']")
	WebElement languageBtn;
	
	@FindBy(xpath = "(//span[@class = 'uitk-flag flag us']/following::span)[1]")
	WebElement region;
	
	@FindBy(xpath = "(//span[@class = 'uitk-flag flag us']/following::span)[2]")
	WebElement changeRegion;
	
	@FindBy(xpath = "//button[@data-stid = 'picker-submit']")
	WebElement regionDoneBtn;
	
	@FindBy(id = "add-flight-switch")
	WebElement flightCheckBox;
	
	@FindBy(xpath = "//button[@aria-label = 'Leaving from']")
	WebElement leavingFromBtn;
	
	@FindBy(id = "location-field-origin")
	WebElement fromLocation;
	
	@FindBy(xpath = "//button[@aria-label = 'Going to']")
	WebElement goinToBtn;
	
	@FindBy(id = "location-field-destination")
	WebElement toLocation;
	
	@FindBys(@FindBy(xpath = "//*[@id=\"location-field-origin-menu\"]/div[2]/ul"))
	List <WebElement> origin;
	
	@FindBys(@FindBy(xpath = "//*[@id=\"location-field-destination-menu\"]/div[2]/ul"))
	List <WebElement> destination;

	@FindBy(id = "adaptive-menu")
	WebElement selectTravellers;
	
	@FindBy(xpath = "//*[contains(@id,'adult-input')]//preceding::button[1]")
	WebElement decreaseAdults;
	
	@FindBy(id = "adult-input-0")
	WebElement numofAdults;
	
	@FindBy(xpath = "//*[contains(@id,'child-input')]//following::button[1]")
	WebElement inreaseChildren;
	
	@FindBy(xpath = "//*[contains(@id,'child-input')]//preceding::button[1]")
	WebElement decreaseChildren;
	
	@FindBy(id = "child-input-0")
	WebElement numofchildren;
	
	@FindBy(id = "child-age-input-0-0")
	WebElement childAge;
	
	@FindBy(xpath = "//*[contains(text(),'Done')]")
	WebElement travelerDoneButton;
	
	@FindBy(id = "d1-btn")
	WebElement dateSelect;
	
	@FindBy(xpath = "//button[@data-stid = 'apply-date-picker']")
	WebElement dateDone;
	
	@FindBy(xpath = "//button[@data-testid = 'submit-button']")
	WebElement searchButton;
	
	
	
	public ExpediaSearch(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void handleSignInScreen() {
		driver.findElement(By.className("page-header")).click();
	}
	
	//select Language
	public void clickLanguage() {
		languageBtn.click();
	}
	
	//Get text of region 
	public String getRegion() {
		return region.getText();
	}
	
	//Change region if it is not United States
	 public void changeRegion() { 
		 changeRegion.click(); 
		 driver.findElement(By.xpath("(//input[@value = '1'])[2]")).click();
		 driver.findElement(By.xpath("//button[@data-stid = 'apply-change-language']")).click();
	}
	 
	//Region Done Button
	public void clickDoneButton() {
		regionDoneBtn.click();
	}
	
	//Add Flight Checkbox
	public void selectFlightCheckBox () {
		flightCheckBox.click();
	}
	
	//From button
	public void leavingFromBtn () {
		leavingFromBtn.click();
	}
	
	//From Text Box
	public WebElement fromLocationText () {
		return fromLocation;

	}
	
	//To Button
	public void goinToBtn() {
		goinToBtn.click();
	}
	
	//To TextBox
	public WebElement toLocationText () {
		return toLocation;
	}
	
	//Travelers Dropdown
	public void selectTravellers () {
		selectTravellers.click();
	}
	
	//Select number of adult travelers
	public void adultTravellers () {
		String totaladults = numofAdults.getAttribute("value");
		int i = Integer.parseInt(totaladults);
		if (i>1) {
			loop1: for (int a=i; a>=i; a--) {
				decreaseAdults.click();
				String totaladults1 = numofAdults.getAttribute("value");
				int x = Integer.parseInt(totaladults1);
				if (x==1) {
					break loop1;
				}
			}
		}
	}
	
	//Select number of children travelers
	public void childTraveller () {
		String totalchildren = numofchildren.getAttribute("value");
		int i = Integer.parseInt(totalchildren); 
		if (i==0) {
			loop1: for (int a=0; a>=i; a++) {
				inreaseChildren.click();
				String totalchildren1 = numofchildren.getAttribute("value");
				int b = Integer.parseInt(totalchildren1); 
				if (b==1) {
					TestUtil.explicitWait(childAge);
					Select cAge = new Select(childAge);
					cAge.selectByValue("3");
					break loop1;
				}
			}
		}
		else if(i>1)
		{
				loop2: for (int a=0; a>=i; a++) {
					decreaseChildren.click();
				String totalchildren1 = numofchildren.getAttribute("value");
				int b = Integer.parseInt(totalchildren1); 
				if (b==1) {
					TestUtil.explicitWait(childAge);
					Select cAge = new Select(childAge);
					cAge.selectByValue("3");
					break loop2;
				}
			}
		}
	}
	
	//Travelers Done Button
	public void clickOnDone() {
		travelerDoneButton.click();
	}
	
	//Select Date field
	public void dateSelect() {
		dateSelect.click();
		TestUtil.ImplicitWait(5);
		driver.findElement(By.xpath("//button[@aria-label= 'Dec 1, 2020']")).click();
		driver.findElement(By.xpath("//button[@aria-label= 'Dec 20, 2020']")).click();
		dateDone.click();
		
	}
	
	//Search Button
	public ExpediaResult clickSearch() {
		searchButton.click();
		return new ExpediaResult();
	}
	
}
