package com.sitecore.assessment.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import com.sitecore.assessment.utils.TestBase;

public class GoogleResultPage extends TestBase {
	
	@FindBy(name="q") 
	WebElement googlesearchBar;
	
	@FindBy(id = "result-stats")
	WebElement results;
	
	public GoogleResultPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public String getTitle() {
		return driver.getTitle();
	}
	
	public String checkResults() {
		return results.getText();
	}
	
	public WebElement clearSearch() {
		return googlesearchBar;
	}
}
