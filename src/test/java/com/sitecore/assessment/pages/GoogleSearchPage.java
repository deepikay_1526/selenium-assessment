package com.sitecore.assessment.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.sitecore.assessment.utils.TestBase;

public class GoogleSearchPage extends TestBase {
		
	@FindBy(name="q") 
	WebElement googlesearchBar;
	 
	
	@FindBy(name="btnK")
	WebElement googlesearchButton;
	
	public GoogleSearchPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public WebElement searchBahamas() {
		return  googlesearchBar;
	}
	
	public GoogleResultPage clickSearch() {
		googlesearchButton.click();
		return new GoogleResultPage(driver);
	}

}
