package com.sitecore.assessment.testrunner;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import com.cucumber.listener.Reporter;

import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/java/com/sitecore/assessment/features/"
		,glue = {"com.sitecore.assessment.stepdefinitions"},
		plugin = {"pretty", "html:test-output/HtmlReport", "json:test-output/json_output/cucumber.json", "junit:test-output/junit_xml/cucumber.xml","com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"},
		monochrome = true,
		dryRun = false 
		)

public class TestRunner {
	@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig("config/extent-config.xml");
	}
    }



