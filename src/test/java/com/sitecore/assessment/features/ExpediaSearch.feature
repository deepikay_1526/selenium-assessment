Feature: Expedia Ticket Booking

Scenario: To book flight and accomodation in Expedia

Given I navigate to Expedia website
When I look for a flight+accomodation from Brussels to New York
	| Bru | JFK |
Then The result page contains travel option for chosen destination
Then quit the browser session

#Alternatively test data can be passed from excel sheet using apache poi