Feature: Google Search

Scenario Outline: To Search Countries in Google

Given the browser is launched
When Google url is opened
Then search "<country1>" in google search bar
Then click on Search button
Then verify Result page is displayed 
Then take screenshot of Result Page
Then clear the existing search
Then search for "<country2>"
Then quit the browser session


Examples:
		| country1 | country2 |
		| Bahamas | Amsterdam |


#Alternatively test data can be passed from excel sheet using apache poi