package com.sitecore.assessment.stepdefinitions;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.sitecore.assessment.pages.ExpediaResult;
import com.sitecore.assessment.pages.ExpediaSearch;
import com.sitecore.assessment.utils.TestBase;
import com.sitecore.assessment.utils.TestUtil;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ExpediaStepDefinition extends TestBase {
	
	ExpediaSearch expSearch;
	ExpediaResult expResult;

	@Given("^I navigate to Expedia website$")
	public void i_navigate_to_Expedia_website() throws InterruptedException {
		TestBase.initialisation();
	    TestBase.launchURL("url2");
	    TestUtil.ImplicitWait(3);
	}

	@When("^I look for a flight\\+accomodation from Brussels to New York$")
	public void i_look_for_a_flight_accomodation_from_Brussels_to_New_York(DataTable flightData) throws InterruptedException {
		List<List<String>> Flidata = flightData.raw();
		expSearch = new ExpediaSearch(driver);
		expSearch.handleSignInScreen();
		
		//To verify site is running on US
		expSearch.clickLanguage();
		String region = expSearch.getRegion();
		if(region.contains("United States")) {
	    	System.out.println("Selected region is US");
	    }
	    else {
	    	expSearch.changeRegion();
	    	System.out.println("Selected region changed to US");
	    }
		expSearch.clickDoneButton();
		
		//Add Flight Check Box
		expSearch.selectFlightCheckBox();
		
		//Select To Location
		expSearch.goinToBtn();
		TestUtil.ImplicitWait(10);
		WebElement to = expSearch.toLocationText();
		TestUtil.explicitWait(to);
		to.sendKeys(Flidata.get(0).get(1));
		TestUtil.ImplicitWait(10);
		WebElement destUL = driver.findElement(By.xpath("//*[@id='location-field-destination-menu']/div[2]/ul"));
		TestUtil.explicitWait(destUL);
		List<WebElement> destLI = destUL.findElements(By.tagName("li"));
		for (WebElement dli : destLI) {
			if (dli.getText().contains("John F. Kennedy Intl")) {
				dli.click();
			}
		Thread.sleep(3000);
		}
		
		//Select From Location
		expSearch.leavingFromBtn();
		TestUtil.ImplicitWait(10);
		WebElement from = expSearch.fromLocationText();
		TestUtil.explicitWait(from);
		from.sendKeys(Flidata.get(0).get(0));
		TestUtil.ImplicitWait(10);
		WebElement originUL = driver.findElement(By.xpath("//*[@id='location-field-origin-menu']/div[2]/ul"));
		TestUtil.explicitWait(originUL);
		List <WebElement> originLI = originUL.findElements(By.tagName("li"));
		for (WebElement uli : originLI)
		{
			if(uli.getText().contains("Brussels-National")) {
				uli.click();
		}
		}
		
				
		//Select Travelers
		expSearch.selectTravellers();
		TestUtil.ImplicitWait(3);
		expSearch.adultTravellers();
		TestUtil.ImplicitWait(3);
		expSearch.childTraveller();
		TestUtil.captureScreenShot(driver);
		expSearch.clickOnDone();
		TestUtil.ImplicitWait(2);
		
		//Select Date
		expSearch.dateSelect();
		TestUtil.ImplicitWait(5);
		TestUtil.captureScreenShot(driver);
		expResult = expSearch.clickSearch();
	}

	@Then("^The result page contains travel option for chosen destination$")
	public void the_result_page_contains_travel_option_for_chosen_destination() throws InterruptedException {
		String resultTitle = expResult.verifySearchResults();
		Thread.sleep(8000);
		if(resultTitle.contains("New York")) {
		System.out.println("Results found for New York");	
		}
		
		
	}


}
