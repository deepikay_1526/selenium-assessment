package com.sitecore.assessment.stepdefinitions;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;


import com.sitecore.assessment.pages.GoogleResultPage;
import com.sitecore.assessment.pages.GoogleSearchPage;
import com.sitecore.assessment.utils.TestBase;
import com.sitecore.assessment.utils.TestUtil;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GoogleStepDefinition extends TestBase {
	
	GoogleSearchPage gsp;
	GoogleResultPage grp;
	WebElement search;
	
	@Given("^the browser is launched$")
	public void the_browser_is_launched() {
		TestBase.initialisation();
			    
	}
	  @When("^Google url is opened$") 
	  public void google_url_is_opened() {
		  TestBase.launchURL("url1"); 
		  TestUtil.handlingInconsistentCookieWindow();
	}
	 
	@Then("^search \"(.*)\" in google search bar$")
	public void search_Bahamas_in_google_search_bar(String country1) {
		TestUtil.ImplicitWait(2);
		gsp = new GoogleSearchPage(driver);
		WebElement search = gsp.searchBahamas();
		search.sendKeys(country1);
	}

	@Then("^click on Search button$")
	public void click_on_Search_button() {
		grp = gsp.clickSearch();
	}

	@Then("^verify Result page is displayed$")
	public void verify_Result_page_is_displayed() {	    
	    String result = grp.checkResults();
	    if(result.contains("results")) {
	    	System.out.println("User is on Results page");
	    }
	    else {
	    	System.out.println("Result page is not displayed");
	    }
	}
	
	
	  @Then("^take screenshot of Result Page$") 
	  public void take_screenshot_of_Result_Page() {
		  TestUtil.captureScreenShot(driver);
	  
	  }
	 

	@Then("^clear the existing search$")
	public void clear_the_existing_search() {
		search = grp.clearSearch();
	    search.clear();
	    
	}

	@Then("^search for \"(.*)\"$")
	public void search_for_Amsterdam(String country2) {
	    search.sendKeys(country2);
	    search.sendKeys(Keys.ENTER);
	    TestUtil.ImplicitWait(2);
	    TestUtil.captureScreenShot(driver);
	}
	
	@Then("^quit the browser session$")
	public void quit_the_browser_session() {
	    TestUtil.quitBroswer();
	    
	}



}
