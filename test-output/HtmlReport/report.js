$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ExpediaSearch.feature");
formatter.feature({
  "line": 1,
  "name": "Expedia Ticket Booking",
  "description": "",
  "id": "expedia-ticket-booking",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "To book flight and accomodation in Expedia",
  "description": "",
  "id": "expedia-ticket-booking;to-book-flight-and-accomodation-in-expedia",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I navigate to Expedia website",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I look for a flight+accomodation from Brussels to New York",
  "rows": [
    {
      "cells": [
        "Bru",
        "JFK"
      ],
      "line": 7
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "The result page contains travel option for chosen destination",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "quit the browser session",
  "keyword": "Then "
});
formatter.match({
  "location": "ExpediaStepDefinition.i_navigate_to_Expedia_website()"
});
formatter.result({
  "duration": 17422275000,
  "status": "passed"
});
formatter.match({
  "location": "ExpediaStepDefinition.i_look_for_a_flight_accomodation_from_Brussels_to_New_York(DataTable)"
});
formatter.result({
  "duration": 55954070400,
  "status": "passed"
});
formatter.match({
  "location": "ExpediaStepDefinition.the_result_page_contains_travel_option_for_chosen_destination()"
});
formatter.result({
  "duration": 8012763500,
  "status": "passed"
});
formatter.match({
  "location": "GoogleStepDefinition.quit_the_browser_session()"
});
formatter.result({
  "duration": 1453178000,
  "status": "passed"
});
formatter.uri("GoogleSearch.feature");
formatter.feature({
  "line": 1,
  "name": "Google Search",
  "description": "",
  "id": "google-search",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "To Search Countries in Google",
  "description": "",
  "id": "google-search;to-search-countries-in-google",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "the browser is launched",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Google url is opened",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "search \"\u003ccountry1\u003e\" in google search bar",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "click on Search button",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "verify Result page is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "take screenshot of Result Page",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "clear the existing search",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "search for \"\u003ccountry2\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "quit the browser session",
  "keyword": "Then "
});
formatter.examples({
  "line": 16,
  "name": "",
  "description": "",
  "id": "google-search;to-search-countries-in-google;",
  "rows": [
    {
      "cells": [
        "country1",
        "country2"
      ],
      "line": 17,
      "id": "google-search;to-search-countries-in-google;;1"
    },
    {
      "cells": [
        "Bahamas",
        "Amsterdam"
      ],
      "line": 18,
      "id": "google-search;to-search-countries-in-google;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 18,
  "name": "To Search Countries in Google",
  "description": "",
  "id": "google-search;to-search-countries-in-google;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "the browser is launched",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Google url is opened",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "search \"Bahamas\" in google search bar",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "click on Search button",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "verify Result page is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "take screenshot of Result Page",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "clear the existing search",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "search for \"Amsterdam\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "quit the browser session",
  "keyword": "Then "
});
formatter.match({
  "location": "GoogleStepDefinition.the_browser_is_launched()"
});
formatter.result({
  "duration": 6050228400,
  "status": "passed"
});
formatter.match({
  "location": "GoogleStepDefinition.google_url_is_opened()"
});
formatter.result({
  "duration": 4283304900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bahamas",
      "offset": 8
    }
  ],
  "location": "GoogleStepDefinition.search_Bahamas_in_google_search_bar(String)"
});
formatter.result({
  "duration": 143907500,
  "status": "passed"
});
formatter.match({
  "location": "GoogleStepDefinition.click_on_Search_button()"
});
formatter.result({
  "duration": 2177483300,
  "status": "passed"
});
formatter.match({
  "location": "GoogleStepDefinition.verify_Result_page_is_displayed()"
});
formatter.result({
  "duration": 96699700,
  "status": "passed"
});
formatter.match({
  "location": "GoogleStepDefinition.take_screenshot_of_Result_Page()"
});
formatter.result({
  "duration": 914159700,
  "status": "passed"
});
formatter.match({
  "location": "GoogleStepDefinition.clear_the_existing_search()"
});
formatter.result({
  "duration": 176202300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Amsterdam",
      "offset": 12
    }
  ],
  "location": "GoogleStepDefinition.search_for_Amsterdam(String)"
});
formatter.result({
  "duration": 2564684000,
  "status": "passed"
});
formatter.match({
  "location": "GoogleStepDefinition.quit_the_browser_session()"
});
formatter.result({
  "duration": 1276147700,
  "status": "passed"
});
});