﻿# Selenium Assessment #
Automated Test Java with Cucumber and Selenium WebDriver
This project is an UI automated functional test for Google Search and Expedia website.
Test scenarios are described in the feature files located inside the project.

## Installation ##
You need to have [Java 8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)  JDK installed along with [Maven](https://maven.apache.org/install.html).

To run the tests locally with [ChromeDriver](https://chromedriver.chromium.org/getting-started), download updated version and add it inside drivers folder available in the project.

To run the tests locally with [InternetExplorer](https://www.selenium.dev/downloads/), download updated version and add it inside drivers folder available in the project.
Note: Download and update drivers only if required.

## Install Dependencies ##
```console
$ mvn clean install
```
## Running test ##
    
```console
$ mvn test
```
By default, tests will run on Chrome to change the browser to internet explorer edit the config file inside the project and set browser=ie

## Running test in IDE ##
You can import the project into your desired IDE.
Navigate and execute the TestRunner.java avilable in the below path src/test/java/com/sitecore/assessment/testrunner.java

## Extend Report ##
The cucumber HTML report can be found within the project- target/cucumber-reports/report.html

![Result](img/Result.PNG)